#coding=utf-8
# https://dev.dota2.com/showthread.php?t=58317
# https://github.com/kronusme/dota2-api/blob/master/data/heroes.json
# https://api.opendota.com/api/heroes
from __future__ import print_function
try:
    from urllib.request import urlopen, Request
except ImportError:
    from urllib2 import urlopen, Request

import os
import argparse
from json import loads, dumps
import xml.etree.ElementTree


def update_progress(progress, max_v):
    print('\r[{0}] {1:.2f}% '.format('#'*int(progress/10), progress*100/max_v), end="")


class Parser(object):
    def __init__(self):
        self.heroes = None
        self.sb_path = "sb"
        self.head_file = "../parsers.h"
        self.cdn = 'http://cdn.steamstatic.com/apps/dota2/images/heroes/{}_{}'
        self.load_heroes_json()

    def load_heroes_json(self):
        req = Request("https://api.opendota.com/api/heroes",headers={'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36'}) 
        if not urlopen(req).getcode() == 200:
            print("Cant load JSON file")
            exit()
        self.heroes = loads(urlopen(req).read())

    def load_icons_png(self, size):
        k = 0
        sb_path = size + ".png"
        if not os.path.exists(os.path.join(os.getcwd(), size)):
            os.mkdir(os.path.join(os.getcwd(), size))
        for i in self.heroes:
            k+=1
            update_progress(k, len(self.heroes))
            name = i['name'].replace("npc_dota_hero_", '')

            with open('{}/{}{}'.format(size, name, ".png"), "wb") as png:
                req = urlopen(self.cdn.format(name, sb_path))
                if not req.getcode() == 200:
                    print("File at %s not available" % req.geturl())
                    continue
                png.write(req.read())
        print("\nOut folder: {}".format(os.path.join(os.getcwd(), size)))
        
    def get_lg_png(self):
        print("Loading large size file")
        self.load_icons_png("lg")

    def get_sb_png(self):
        print("Loading small size file")
        self.load_icons_png("sb")

    def generate(self):
        print("Generate {} file".format(os.path.abspath(self.head_file)))

        ids = [{i["localized_name"]:i["id"]} for i in self.heroes]
        map_str = dumps(ids, sort_keys=True, indent=4).replace(':', ',').strip("[]")
        with open(self.head_file, "r") as f:
            file = f.read()
        parts = file.split("//#python>")
        if len(parts) != 3:
            print("""Your h-file has wrong tag structure.\n"""
                  """Please edit him something like this example:\n"""
                  """//********** h-file top **********//\n\n"""
                  """const std::map <const char*, int, charcomp> heroes = {\n"""
                  """   //#python> <-that is start tag\n"""
                  """   //#python> <-that is end tag\n"""
                  """};\n\n"""
                  """//******** h-file bottom *********//""")
            exit()
        parts[1] = map_str
        with open(self.head_file, "w") as f:
            f.write("//#python>".join(parts))
        print("{} records was been add to {} file".format(len(self.heroes), os.path.abspath(self.head_file)))

    def resourse(self):
        """
        <qresource prefix="/">
        <file alias="14">hero_icon_parser/lg/abaddon.png</file>
        <file alias="12">hero_icon_parser/lg/abyssal_underlord.png</file>
        <file alias="76">hero_icon_parser/lg/alchemist.png</file>
        """

        resfile = '../pesource.qrc'
        pref ='hero_icon_parser/lg/{}.png'

        e = xml.etree.ElementTree.parse(resfile)
        root = e.getroot()

        for res_tag in root.findall('qresource'):
            if res_tag.attrib['prefix'] == "/":
                for ch in list(res_tag):
                    res_tag.remove(ch)
                for i in self.heroes:
                    attr = {'alias': str(i['id'])}
                    record = xml.etree.ElementTree.SubElement(res_tag, 'file', attr)
                    record.text = pref.format(i['name'].replace("npc_dota_hero_", ''))
                e.write(resfile)
                return
        else:
            print("cant find root elements for hero section")
            exit()

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument('-l', dest="large", action='store_true',
                        help='Load large heroes pictures from dota CDN', default=False)
    parser.add_argument('-s', dest="small", action='store_true',
                        help='Load small heroes pictures from dota CDN', default=False)
    parser.add_argument('-g', dest="generate", action='store_true',
                        help='Generate h-file with hero name => id, for map', default=False)
    parser.add_argument('-r', dest="resourse", action='store_true',
                        help='Generate QRC file with hero image and aliases', default=False)
    args = parser.parse_args()
    tool = Parser()
    if args.resourse:
        tool.resourse()
    if args.large:
        tool.get_lg_png()
    if args.small:
        tool.get_sb_png()
    if args.generate:
        tool.generate()
