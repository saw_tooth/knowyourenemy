#pragma once

#include "gumbo/src/gumbo.h"
#include <cstring>
#include <vector>
#include <queue>


class GumboParser

{
public:
    GumboParser(const char* html);
    ~GumboParser();
protected:
    const GumboNode* findByTagName(GumboTag tag) const ;
    const GumboNode* findByTagName(const GumboNode* r, GumboTag tag) const;

    const GumboNode* findByTagNameBFS(GumboTag tag) const;
    const GumboNode* findByTagNameBFS(const GumboNode* r, GumboTag tag) const;

    void findAllByTagName(GumboTag tag, std::vector<const GumboNode *> &vec) const;
    void findAllByTagName(const GumboNode* r, GumboTag tag, std::vector<const GumboNode *> &vec) const;

    void findAllByTagNameBFS(const GumboNode* r, GumboTag tag, std::vector<const GumboNode *> &vec) const;
    void findAllByTagNameBFS(GumboTag tag, std::vector<const GumboNode *> &vec) const;

    const GumboNode* findByClassName(const char* cls_name) const;
    const GumboNode* findByClassName(const GumboNode* r, const char* cls_name) const;

    void findAllByClassName(const char* cls_name, std::vector<const GumboNode*> &vec) const;
    void findAllByClassName(const GumboNode* r, const char* cls_name, std::vector<const GumboNode*> &vec) const;

    const GumboNode* findById(const char* id_name) const;
    const GumboNode* findById(const GumboNode* r, const char* id_name) const;

    void findAllById(const char* id_name, std::vector<const GumboNode*> &vec) const;
    void findAllById(const GumboNode* r, const char* id_name, std::vector<const GumboNode*> &vec) const;


    const char* getId(const GumboElement* el) const;
    const char* getId(const GumboNode* node) const;

    const char* getText(const GumboElement* el) const;
    const char* getText(const GumboNode* node) const;

    const char* getLink(const GumboElement* el) const;
    const char* getLink(const GumboNode* node) const;

    const char* getClass(const GumboElement* el) const;
    const char* getClass(const GumboNode* node) const;

    const char* getAttribute(const GumboElement* el, const char *attr) const;
    const char* getAttribute(const GumboNode* node, const char *attr) const;

private:
    GumboOutput* obj;
    GumboNode* document;
};
