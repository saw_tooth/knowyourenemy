#pragma once
#include "windows.h"
#include "winuser.h"
#include <QMainWindow>
#include <QDebug>

void registerGlobalKeys(QMainWindow *hnd);
void unregisterGlobalKeys(QMainWindow *hnd);
