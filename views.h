#pragma once

#include <QString>
#include <QObject>
#include <QByteArray>
#include <QWidget>
#include <QUrl>
#include <QPixmap>
#include <QVBoxlayout>
#include <QHBoxlayout>
#include <QPalette>
#include <QLabel>
#include <QPainter>
#include <QColor>
#include <QStackedWidget>
#include "parsers.h"
#include "settings.h"

const int color_pallete[4] = {0x2323cc, 0xFF0000, 0x3CB371, 0xC0C0C0};

class HeroIcon: public QLabel
{
public:
    HeroIcon(QWidget* parent = nullptr);
    int h_idx = 0;
    void setIcon(int idx);
    virtual void resize();
    void resizeEvent(QResizeEvent *event);
};

class GameIcon: public HeroIcon
{
public:
    GameIcon(QWidget* parent =nullptr);
    int status = 0;
    void setStatus(int value);
    void resize();
};

class LastGameView: public QStackedWidget
{
private:
    QLabel*     empty;
    QWidget*    container;
    GameIcon*   icon;
    QLabel*     kda;
    void ui();
public:
    LastGameView();
    void setEmpty();
    void set(const match& value);
    void setUI(const bool& g_on, const bool& kda);
};

class MostHeroView:public QStackedWidget
{
private:
    QLabel* empty;
    QWidget* container;
    HeroIcon* icon;
    QLabel* kda;
    QLabel* winrate;
    QLabel* match_played;
    void ui();
public:
    MostHeroView();
    void setEmpty();
    void set(const hero& value);
    void setUI(const bool& h_on, const bool& match, const bool& kda, const bool& winrate);
};

class Player: public QWidget
{

private:
    bool isPrivate = false;
    int id = 0;
    QVBoxLayout vlMain;

    QLabel*     lSummaryInfo[3] = {nullptr};
    QLabel*     lPartialInfo[3] = {nullptr};

    LastGameView* lastGames[3] = {nullptr};
    MostHeroView* mostHeroes[3] = {nullptr};


    QLabel* qlPartIndex = nullptr;
    QLabel* qlSummIndex = nullptr;

    QLabel* qlName = nullptr;

    void ui();

public slots:
    void upplySettings();
public:
    Player(QWidget* parent = 0);

    void updatePlayerInfo(PlayerInfoParser& parser);
    void updateMatchesInfo(MatchesInfoParser& parser);
    void updateHeroesInfo(HeroesInfoParser& parser);
    void updatePeriodMatchesInfo(PeriodGameStatParser& parser);

    void setName(const char* value);
    void setSumWR(float value);
    void setSumWin(int value);
    void setSumLose(int value);
    void setPartWR(float value);
    void setPartWin(int value);
    void setPartLose(int value);

#ifdef QT_DEBUG
    void debugInfo();
    void debugMatches();
    void debugHeroes();
#endif

    bool getIsPrivate() const;
    void setIsPrivate(bool value);
    void setMostHeroesItem(const hero& value, int view_idx);
    void setLastGamesItem(const match& value, int view_idx);
};
