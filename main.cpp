#include "mainwindow.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QFile theme_style(":/qdarkstyle/style.qss");
    QFile custom_style(":/av/style.qss");
    custom_style.open(QFile::ReadOnly);
    theme_style.open(QFile::ReadOnly);
    QString styleSheet = QLatin1String(theme_style.readAll()) + QLatin1String(custom_style.readAll());
    a.setStyleSheet(styleSheet);
    a.setQuitOnLastWindowClosed(false);
    custom_style.close();
    theme_style.close();

    MainWindow w;
    registerGlobalKeys(&w);

    return a.exec();
}
