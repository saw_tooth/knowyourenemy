#pragma once

#define INI_FILE "setting.ini"

#include <QDebug>
#include <QWidget>
#include <QGroupBox>
#include <QGridLayout>
#include <QFileDialog>
#include <QCheckBox>
#include <QLabel>
#include <QComboBox>
#include <QPushButton>
#include <QList>
#include <QKeySequenceEdit>
#include <QKeyEvent>
#include <QSettings>
#include <QSizePolicy>
#include <QApplication>
#include <QDesktopWidget>

class SettingsWidget : public QWidget
{
    Q_OBJECT
private:
    QVBoxLayout mainContainer;
    QGroupBox* gbGeneral;
    QLabel* gStatFolder;
    QGroupBox* gbMain;
    QGroupBox* gbGame;
    QGroupBox* gbHero;

    QGridLayout* glGeneral;
    QGridLayout* glMain;
    QGridLayout* glGame;
    QGridLayout* glHero;

    QFileDialog* fdSteamPath;
    QPushButton* bPathSelect;
    QLabel* steamLibraryPath;
    QString dir;
    QKeySequenceEdit* hideHotKey;
    QKeySequenceEdit* closeHotKey;


    QCheckBox* chbSummGames;
    QCheckBox* chbPeriodGames;
    QComboBox* cbPeriodDays;

    QCheckBox* chbEnblExpGpmLastGames;
    QCheckBox* chbEnblKDALastGames;

    QComboBox* cbPeriodDaysHero;
    QCheckBox* chbEnblMatchedHero;
    QCheckBox* chbEnblWRHero;
    QCheckBox* chbEnblExpGpmHero;
    QCheckBox* chbEnblKDAHero;

    QPushButton* bSave;

    void generalSettings();
    void mainInfomationSettings();
    void gameSettings();
    void heroSettings();
    void saveButton();
    void setWindowsView();
    void onOpenSettings();
    void showEvent(QShowEvent *event) override;

public:
    explicit SettingsWidget(QWidget* parent = nullptr);
    static void cutTextForLabel(QString& text, QLabel* label);
    ~SettingsWidget();

private slots:
    void saveNewSettings();
    void onChangeSteamFolderPath();
signals:
    void save();
};
