#-------------------------------------------------
#
# Project created by QtCreator 2017-11-30T19:58:40
#
#-------------------------------------------------
QMAKE_TARGET_COMPANY = "YAL Inc"
QMAKE_TARGET_DESCRIPTION = "Dota2 helper"
QMAKE_TARGET_COPYRIGHT = "saw_tooth"
QMAKE_TARGET_PRODUCT = "KnowYourEnemy"

VERSION = 0.1.2.2
QT       += core gui widgets network
TARGET = KnowYourEnemy
TEMPLATE = app

CONFIG += c++11
CONFIG += resources_big

SOURCES += \
        main.cpp \
        mainwindow.cpp \
        keyEventler.cpp \
        gumbo/src/attribute.c \
        gumbo/src/char_ref.c \
        gumbo/src/error.c \
        gumbo/src/parser.c \
        gumbo/src/string_buffer.c \
        gumbo/src/string_piece.c \
        gumbo/src/tag.c \
        gumbo/src/tokenizer.c \
        gumbo/src/utf8.c \
        gumbo/src/util.c \
        gumbo/src/vector.c \
        dota2process/dotaclient.cpp \
        parsers.cpp \
        settings.cpp \
        views.cpp \
        dom.cpp


HEADERS += \
        mainwindow.h \
        keyEventler.h \
        gumbo/src/gumbo.h \
        dota2process/dotaclient.h \
        parsers.h \
        settings.h \
        views.h \
        dom.h


INCLUDEPATH += gumbo/visualc/include

win32:RC_ICONS += hero_icon_parser/Know-your-enemy.ico

DISTFILES += \
    hero_icon_parser/parser.py
    bitbucket-pipelines.yml


RESOURCES += \
    pesource.qrc \
    qdark/qdarkstyle/style.qrc
TRANSLATIONS += _ru.ts
