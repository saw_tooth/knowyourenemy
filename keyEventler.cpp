#include "keyEventler.h"

void registerGlobalKeys(QMainWindow *hnd)
{
    HWND handle = HWND(hnd->winId());

    if(!RegisterHotKey(handle, 0, MOD_ALT | MOD_CONTROL, VK_NUMPAD1))
    {
        qDebug() << "Cant register hotkey";
    };
    if(!RegisterHotKey(handle, 1, MOD_ALT | MOD_CONTROL, VK_NUMPAD2))
    {
        qDebug() << "Cant register hotkey";
    };
    if(!RegisterHotKey(handle, 2, MOD_ALT | MOD_CONTROL, VK_NUMPAD3))
    {
        qDebug() << "Cant register hotkey";
    };
}

void unregisterGlobalKeys(QMainWindow *hnd)
{
    HWND handle = HWND(hnd->winId());

    if(!UnregisterHotKey(handle, 0))
    {
        qDebug() << "Cant unregister hotkey";
    };
    if(!UnregisterHotKey(handle, 1))
    {
        qDebug() << "Cant unregister hotkey";
    };
    if(!UnregisterHotKey(handle, 2))
    {
        qDebug() << "Cant unregister hotkey";
    };
}
