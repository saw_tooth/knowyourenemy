#include "settings.h"
#include <QMessageBox>
SettingsWidget::SettingsWidget(QWidget* parent) : QWidget(parent)
{
    //setWindowFlags(Qt::FramelessWindowHint);
    generalSettings();
    mainInfomationSettings();
    gameSettings();
    heroSettings();
    saveButton();
    setWindowsView();
}
SettingsWidget::~SettingsWidget()
{

}
void SettingsWidget::generalSettings()
{
    gbGeneral = new QGroupBox(tr("General"));
    glGeneral = new QGridLayout;
    steamLibraryPath = new QLabel(this);

    bPathSelect = new QPushButton(tr("Select Folder..."));
    connect(bPathSelect, SIGNAL(clicked()), this, SLOT(onChangeSteamFolderPath()));
    gStatFolder = new QLabel(this);
    gStatFolder->setPixmap(QPixmap(":/av/nok").scaledToHeight(gStatFolder->height() - 10, Qt::SmoothTransformation));

    hideHotKey = new QKeySequenceEdit(this);
    closeHotKey = new QKeySequenceEdit(this);

    glGeneral->addWidget(new QLabel(tr("Steam Libraly Path:")), 0,0,1,2);
    glGeneral->addWidget(gStatFolder, 0,2,1,1);
    glGeneral->addWidget(bPathSelect, 1,0,1,2);
    glGeneral->addWidget(steamLibraryPath, 1,2,1,6);
    glGeneral->addWidget(new QLabel(tr("Hide hotkey")), 2,0,1,2);
    glGeneral->addWidget(hideHotKey, 2,2,1,6);
    glGeneral->addWidget(new QLabel(tr("Close hotkey")), 3,0,1,2);
    glGeneral->addWidget(closeHotKey, 3,2,1,6);

    gbGeneral->setLayout(glGeneral);
    mainContainer.addWidget(gbGeneral);
}
void SettingsWidget::mainInfomationSettings()
{
    gbMain = new QGroupBox(tr("Enable Main Information View"));
    gbMain->setCheckable(true);

    chbSummGames = new QCheckBox(tr("Displayed Summary Games Stat"));
    chbPeriodGames = new QCheckBox(tr("Displayed Period Games Stat"));
    cbPeriodDays = new QComboBox();
    cbPeriodDays->addItems({tr("Week"), tr("Month"), tr("3 Month"), tr("6 Month")});

    glMain = new QGridLayout;
    glMain->addWidget(chbSummGames, 0,0,1,8);
    glMain->addWidget(chbPeriodGames, 1,0,1,8);
    glMain->addWidget(cbPeriodDays, 2,0,1,2);
    glMain->addWidget(new QLabel(tr("Period Games Stat")), 2,2,1,6);

    gbMain->setLayout(glMain);
    mainContainer.addWidget(gbMain);
}
void SettingsWidget::gameSettings()
{
    gbGame = new QGroupBox(tr("Enable Last Games Information View"));
    gbGame->setCheckable(true);
    glGame = new QGridLayout;

    chbEnblExpGpmLastGames = new QCheckBox(tr("Displayed GPM/XPM"));
    chbEnblKDALastGames = new QCheckBox(tr("Displayed KDA"));

    glGame = new QGridLayout;
    glGame->addWidget(chbEnblKDALastGames, 0,0,1,8);
    glGame->addWidget(chbEnblExpGpmLastGames, 1,0,1,8);

    gbGame->setLayout(glGame);
    mainContainer.addWidget(gbGame);
}
void SettingsWidget::heroSettings()
{
    gbHero = new QGroupBox(tr("Enable Most Heroes Information View"));
    gbHero->setCheckable(true);
    glHero = new QGridLayout;

    cbPeriodDaysHero = new QComboBox();
    cbPeriodDaysHero->addItems({tr("All Time"), tr("Week"), tr("Month"), tr("3 Month"), tr("6 Month")});
    chbEnblMatchedHero = new QCheckBox(tr("Hero Match Played"));
    chbEnblKDAHero = new QCheckBox(tr("Hero KDA"));
    chbEnblWRHero = new QCheckBox(tr("Hero Winrate"));
    chbEnblExpGpmHero = new QCheckBox(tr("Hero GPM/XPM"));

    glHero->addWidget(cbPeriodDaysHero, 0,0,1,2);
    glHero->addWidget(new QLabel(tr("Period Heroes Stat")), 0,2,1,6);
    glHero->addWidget(chbEnblMatchedHero, 1,0,1,8);
    glHero->addWidget(chbEnblKDAHero, 2,0,1,8);
    glHero->addWidget(chbEnblWRHero, 3,0,1,8);
    glHero->addWidget(chbEnblExpGpmHero, 4,0,1,8);

    gbHero->setLayout(glHero);
    mainContainer.addWidget(gbHero);
}

void SettingsWidget::setWindowsView()
{
    setWindowTitle(tr("Settings"));
    setLayout(&mainContainer);
    setAttribute(Qt::WA_QuitOnClose, false);
    QSize mSize = this->sizeHint();
    QRect screenRect = QDesktopWidget().screen()->rect();
    move(QPoint(screenRect.width()/2 - mSize.width()/2,
                        screenRect.height()/2 - mSize.height()/2));
}

void SettingsWidget::saveButton()
{
    bSave = new QPushButton(tr("Save"));
    mainContainer.addWidget(bSave, Qt::AlignRight);
    connect(bSave, SIGNAL(clicked()), this, SLOT(saveNewSettings()));
}

//SLOTS
void SettingsWidget::onChangeSteamFolderPath()
{
    dir = QFileDialog::getExistingDirectory(this, tr("Select Steam Libraly"), dir);
    cutTextForLabel(dir, steamLibraryPath);
}

void SettingsWidget::saveNewSettings()
{
    qDebug() << "Saving Preferences";
    QSettings set(INI_FILE, QSettings::IniFormat);

    set.setValue("General/steam_path", dir);
    set.setValue("General/hide_shortcut", hideHotKey->keySequence().toString());
    set.setValue("General/close_shortcut", closeHotKey->keySequence().toString());

    set.setValue("MainView/display_view", gbMain->isChecked());
    set.setValue("MainView/summary_view", chbSummGames->isChecked());
    set.setValue("MainView/period_view", chbPeriodGames->isChecked());
    set.setValue("MainView/period_value", cbPeriodDays->currentIndex());

    set.setValue("GamesView/display_view", gbGame->isChecked());
    set.setValue("GamesView/kda_view", chbEnblKDALastGames->isChecked());
    set.setValue("GamesView/xpm_gpm_view", chbEnblExpGpmLastGames->isChecked());

    set.setValue("HeroesView/display_view", gbHero->isChecked());
    set.setValue("HeroesView/period_value", cbPeriodDaysHero->currentIndex());
    set.setValue("HeroesView/matches_view", chbEnblMatchedHero->isChecked());
    set.setValue("HeroesView/kda_view", chbEnblKDAHero->isChecked());
    set.setValue("HeroesView/winrate_view", chbEnblWRHero->isChecked());
    set.setValue("HeroesView/xpm_gpm_view", chbEnblExpGpmHero->isChecked());
    set.sync();
    emit save();
}

void SettingsWidget::onOpenSettings()
{
    qDebug() << "Loading Preferences";
    QSettings set(INI_FILE, QSettings::IniFormat);

    dir = set.value("General/steam_path").toString();
    cutTextForLabel(dir, steamLibraryPath);
    hideHotKey->setKeySequence(QKeySequence::fromString(set.value("General/hide_shortcut").toString()));
    closeHotKey->setKeySequence(QKeySequence::fromString(set.value("General/close_shortcut").toString()));

    gbMain->setChecked(set.value("MainView/display_view").toBool());
    chbSummGames->setChecked(set.value("MainView/summary_view").toBool());
    chbPeriodGames->setChecked(set.value("MainView/period_view").toBool());
    cbPeriodDays->setCurrentIndex(set.value("MainView/period_value").toInt());

    gbGame->setChecked(set.value("GamesView/display_view").toBool());
    chbEnblExpGpmLastGames->setChecked(set.value("GamesView/xpm_gpm_view").toBool());
    chbEnblKDALastGames->setChecked(set.value("GamesView/kda_view").toBool());

    gbHero->setChecked(set.value("HeroesView/display_view").toBool());
    cbPeriodDaysHero->setCurrentIndex(set.value("HeroesView/period_value").toInt());
    chbEnblMatchedHero->setChecked(set.value("HeroesView/matches_view").toBool());
    chbEnblKDAHero->setChecked(set.value("HeroesView/kda_view").toBool());
    chbEnblWRHero->setChecked(set.value("HeroesView/winrate_view").toBool());
    chbEnblExpGpmHero->setChecked(set.value("HeroesView/xpm_gpm_view").toBool());
}

void SettingsWidget::showEvent(QShowEvent* event)
{
    Q_UNUSED(event);
    onOpenSettings();
}
void SettingsWidget::cutTextForLabel(QString& text, QLabel* label)
{
    label->setToolTip(text);
    QFontMetrics fm(label->font());
    if(label->width() < fm.width(text))
    {
        label->setText(label->fontMetrics().elidedText(text, Qt::ElideRight, label->width()));
    }
    else
    {
        label->setText(text);
    }
}
