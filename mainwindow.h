#pragma once

#include "keyEventler.h"
#include "dota2process/dotaclient.h"
#include "settings.h"
#include "views.h"
#include "parsers.h"
#include <QDesktopWidget>
#include <QRect>
#include <QMainWindow>
#include <QApplication>
#include <QTimer>
#include <QObject>
#include <QLabel>
#include <QDebug>
#include <QFile>
#include <QFileSystemWatcher>
#include <QRegularExpression>
#include <QRegularExpressionMatch>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QUrl>
#include "QVBoxLayout"
#include <QSystemTrayIcon>
#include <QAction>
#include <QMenu>
#include <QMessageBox>

enum Action
{
    MAIN_INFORMATION,
    PERIOD_MATCHES,
    LAST_MATCHES_INFORMATION,
    HEROES_INFORMATION
};
static const QString game_period[4] = {"week", "month", "3month", "6month"};
static const QString hero_period[5] = {"","week", "month", "3month", "6month"};

class MainWindow : public QMainWindow
{
    Q_OBJECT

private:
    SettingsWidget settings;
    QFileSystemWatcher filewatcher;
    QNetworkAccessManager network;
    QSystemTrayIcon tray;
    QMenu trayMenu;
    QAction actionSettingsOpen;
    QAction actionQuitApplication;
    QAction actionAbout;

    QWidget mainWidget;
    QHBoxLayout mainLayout;

    bool nativeEvent(const QByteArray &eventType, void *message, long *result);
    bool conectServerLogFile(QString server_log_path);
    void parseNewStringFromServerLog(const QString &lastLine);
    void setTrayIcon();

    void parsePlayer(PlayerInfoParser& dataPlayer, int index);
    void parseMatches(MatchesInfoParser& dataMatches, int index);
    void parseHeroes(HeroesInfoParser& dataHeroes, int index);
    void parsePeriodMatches(PeriodGameStatParser& dataPeriodMatches, int index);


    void loadMatchesInfo(int index);
    void loadHeroesInfo(int index);
    void loadPlayerInfo();

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    QList<int> idPlayersList;
    QList<int> idPartyPlayersList;
    Player* objPlayers[10] = {nullptr};

private slots:
    void dotaServerLogAreUpdate(QString server_log_path);
    void dataFromServerWasLoaded(QNetworkReply *dbResponse);
    void readDataFromServer();
    void aboutMessage();
    void upplySettings();

signals:
    void readyToParseHTML(int index);

};

