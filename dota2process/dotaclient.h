#pragma once
#define DOTA_PROCESS_NAME L"dota2.exe"
#define DOTA_SERVER_LOG_FILE "../server_log.txt"

#include "stddef.h"
#include "windows.h"
#include "winuser.h"
#include "TlHelp32.h"
#include <QDebug>

//Return Dota2 process ID from scope, if dota2 not run return nullptr

DWORD getIdProcessByName(const wchar_t *processname);
