#include "mainwindow.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    tray(this),
    trayMenu(this),
    actionSettingsOpen(tr("&Settings"), this),
    actionQuitApplication(tr("&Quit"), this),
    actionAbout(tr("&About"), this)
{
    setWindowFlags(Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint| Qt::WindowTransparentForInput| Qt::WindowDoesNotAcceptFocus);
    setAttribute(Qt::WA_TranslucentBackground);
    setAttribute(Qt::WA_ShowWithoutActivating);
    setAttribute(Qt::WA_DontShowOnScreen);
    setFocusPolicy(Qt::NoFocus);
    QRect rec = QApplication::desktop()->screenGeometry();
    setFixedWidth(rec.width());
    setWindowIcon(QIcon(":av/icon"));
    setTrayIcon();
    DWORD dotaProcessID = getIdProcessByName(DOTA_PROCESS_NAME);
    if (dotaProcessID)
    {
        qDebug() << "Dota are not running. Please run programm after running Dota2";
    }
    if (!conectServerLogFile(DOTA_SERVER_LOG_FILE))
    {
        qDebug() << "Server log file are not found. Please open Settings and enter path to Steam Libraly folder";
        tray.showMessage("Settings is wrong",
                         "Programm wasn't find a correct path to Steam Library.",
                         QSystemTrayIcon::Warning);
    }

    connect(&network, SIGNAL(finished(QNetworkReply *)), this, SLOT(dataFromServerWasLoaded(QNetworkReply *)));
    connect(&settings, &SettingsWidget::save, this, &MainWindow::upplySettings);

    mainLayout.setMargin(5);
    mainLayout.setSpacing(5);

    for(auto &i: objPlayers)
    {
        i = new Player();
        mainLayout.addWidget(i);
    }
    mainWidget.setLayout(&mainLayout);
    setCentralWidget(&mainWidget);
    show();
    hide();
}
bool MainWindow::conectServerLogFile(QString server_log_path)
{
    QFile log_file(server_log_path);
    if (!log_file.exists())
    {
        return false;
    }
    filewatcher.addPath(server_log_path);
    connect(&filewatcher, SIGNAL(fileChanged(QString)), this, SLOT(dotaServerLogAreUpdate(QString)));
    return true;
}

void MainWindow::dotaServerLogAreUpdate(QString server_log_path)
{
    QString last;
    QFile serv_log_file(server_log_path);
    serv_log_file.open(QIODevice::ReadOnly|QIODevice::Text);
    QTextStream stream_log(&serv_log_file);
    do
    {
        last = stream_log.readLine();
    }
    while(!stream_log.atEnd());
    serv_log_file.close();
    qDebug() << "Last string of server log: " << last;
    parseNewStringFromServerLog(last);
}
void MainWindow::parseNewStringFromServerLog(const QString &lastLine)
{
    /* Example of correct string from server_log file.
     *
     * 05/06/2017 - 02:00:13: =[A:1:3655037961:8510]
     * (Lobby 25065313916398394
     * DOTA_GAMEMODE_CM
     *      0:[U:1:217037295]
     *      1:[U:1:111717731]
     *      2:[U:1:113686177]
     *      3:[U:1:348922254]
     *      4:[U:1:411444754]
     *      5:[U:1:125470818]
     *      6:[U:1:160764218]
     *      7:[U:1:130503215]
     *      8:[U:1:287242120]
     *      9:[U:1:105580208])
     * (Party 25065313916127545
     *      0:[U:1:113686177])
     *
    */

    QRegularExpression playerIDRegExp("\\[U\\:\\d\\:([0-9]{5,10})]");       // find one ID Item  2:[U:1:162154381] -> 162154381
    QRegularExpression playerArrayRegExp("\\(Lo.+?\\)");                    // find ID's array part with Lobby players (Lobby...9:[U:1:105580208])
    QRegularExpression partyPlayerArrayRegExp("\\(Par.+?\\)");              // find ID's array part with Party players (Party...0:[U:1:113686177])

    QRegularExpressionMatch players = playerArrayRegExp.match(lastLine);
    QRegularExpressionMatch partyPlayers = partyPlayerArrayRegExp.match(lastLine);

    //Check if line has information about games, or its "loopback"/ "party" trash-string
    if (!players.hasMatch())
    {
        qDebug() << "Server log not have Lobby players array" << lastLine;
        return;
    }
    //parse sub-srtings (Lobby...9:[U:1:105580208]) and get Id into array
    QRegularExpressionMatchIterator playersIterator = playerIDRegExp.globalMatch(players.captured());
    idPlayersList.clear();

    while (playersIterator.hasNext())
    {
        idPlayersList.append(playersIterator.next().captured(1).toInt());
    }
    //if array not have all of players we cant continue and go back, and wait full string
    /*if (!(idPlayersList.length() == 10))
    {
        qDebug() << "Server log has too few players" << idPlayersList;
        idPlayersList.clear();
        return;
    }*/

    //Check if players has party
    if (!partyPlayers.hasMatch())
    {
        qDebug() << "Game without party players" << lastLine;
    }
    else
    {
        QRegularExpressionMatchIterator partyPlayersIterator = playerIDRegExp.globalMatch(partyPlayers.captured());
        idPartyPlayersList.clear();
        while (partyPlayersIterator.hasNext())
        {
            idPartyPlayersList.append(partyPlayersIterator.next().captured(1).toInt());
        }
        if (idPartyPlayersList.length() <= 2)
        {
            qDebug() << "Game has party arrays, but players count is < 2" << idPlayersList;
            idPartyPlayersList.clear();
        }
    }
    //array of players ID was create, time to load data from dotabuff
    loadPlayerInfo();
}
void MainWindow::loadPlayerInfo()
{
    //for each id we need load information from db server, and save byte-data array
    //https://www.dotabuff.com/players/295153316

    QNetworkReply *dbResponse;
    QNetworkRequest dbRequest;
    dbRequest.setAttribute(QNetworkRequest::HttpPipeliningAllowedAttribute, true);

    for (int i=0; i < idPlayersList.length(); i++)
    {
        dbRequest.setUrl(QUrl(QString("https://www.dotabuff.com/players/%1").arg(idPlayersList[i])));

        dbResponse = network.get(dbRequest);
        dbResponse->setProperty("index", i);
        dbResponse->setProperty("action", MAIN_INFORMATION);
    }
}
void MainWindow::dataFromServerWasLoaded(QNetworkReply *dbResponse)
{

    int index = dbResponse->property("index").toInt();
    Action id = static_cast<Action>(dbResponse->property("action").toInt());

    QByteArray data(dbResponse->readAll());

    switch (id)
    {
        case MAIN_INFORMATION:
        {
            PlayerInfoParser plParser(data.constData());
            parsePlayer(plParser, index);
            break;
        }
        case LAST_MATCHES_INFORMATION:
        {

            PeriodGameStatParser matchp(data.constData());
            parsePeriodMatches(matchp, index);
            MatchesInfoParser mtchParser(data.constData());
            parseMatches(mtchParser, index);
            break;
        }
        case HEROES_INFORMATION:
        {
            HeroesInfoParser heroParser(data.constData());
            parseHeroes(heroParser, index);
            break;
        }

    }
    dbResponse->deleteLater();
#if 0
    //get html user for test

    QFile saves(QString("../index_%1_heroes.html").arg(idPlayersList[index]));
    saves.open(QIODevice::WriteOnly|QIODevice::Text);
    saves.write(data);
    saves.flush();
    saves.close();
#endif
}

void MainWindow::readDataFromServer()
{
    /* If the pages are too large, you need to connect to this slot to QNetworkReply::readReady() signals
     * and read the date when it is available:
     * QNetworkReply *dbResponse->readAll(),
     * howewer, prevent need create empty QByteArray objects for html[] in loadDataFromDotabuff() slot,
     * and for each QNetworkReply connect to its slot:
     * connect(dbResponse, SIGNAL(Readyread(), this, SLOT(readDataFromServer()));
     *
     * QNetworkReply *dbResponse = qobject_cast<QNetworkReply*>(sender());
     * int index = dbResponse->property("index").toUInt();
     * html[index] = dbResponse->readAll();
     */
}

void MainWindow::parsePlayer(PlayerInfoParser& dataPlayer, int index)
{
    Player* pl = objPlayers[index];

    pl->updatePlayerInfo(dataPlayer);

#ifdef QT_DEBUG
    pl->debugInfo();
#endif

    if (!pl->getIsPrivate())
    {
        loadMatchesInfo(index);
        loadHeroesInfo(index);
    }
}

void MainWindow::parseMatches(MatchesInfoParser& dataMatches, int index)
{
    Player* pl = objPlayers[index];
    pl->updateMatchesInfo(dataMatches);
}
void MainWindow::parseHeroes(HeroesInfoParser& dataHeroes, int index)
{
    Player* pl = objPlayers[index];
    pl->updateHeroesInfo(dataHeroes);
}

void MainWindow::parsePeriodMatches(PeriodGameStatParser& dataPeriodMatches, int index)
{
    Player* pl = objPlayers[index];
    pl->updatePeriodMatchesInfo(dataPeriodMatches);
}

void MainWindow::loadMatchesInfo(int index)
{
    QNetworkReply *dbResponse;
    QNetworkRequest dbRequest;
    QSettings set(INI_FILE, QSettings::IniFormat);
    int idx = set.value("MainView/period_value").toInt();


    dbRequest.setAttribute(QNetworkRequest::HttpPipeliningAllowedAttribute, true);
    dbRequest.setUrl(QUrl(QString("https://www.dotabuff.com/players/%1/matches?date=%2")
                          .arg(idPlayersList[index])
                          .arg(game_period[idx])));

    dbResponse = network.get(dbRequest);
    dbResponse->setProperty("index", index);
    dbResponse->setProperty("action", LAST_MATCHES_INFORMATION);

}
void MainWindow::loadHeroesInfo(int index)
{
    QNetworkReply *dbResponse;
    QNetworkRequest dbRequest;

    QSettings set(INI_FILE, QSettings::IniFormat);
    int idx = set.value("HeroesView/period_value").toInt();

    dbRequest.setAttribute(QNetworkRequest::HttpPipeliningAllowedAttribute, true);

    dbRequest.setUrl(QUrl(QString("https://www.dotabuff.com/players/%1/heroes?date=%2")
                          .arg(idPlayersList[index])
                          .arg(hero_period[idx])));

    dbResponse = network.get(dbRequest);
    dbResponse->setProperty("index", index);
    dbResponse->setProperty("action", HEROES_INFORMATION);

}
bool MainWindow::nativeEvent(const QByteArray &eventType, void *message, long *result)
{
    Q_UNUSED(eventType);
    Q_UNUSED(result);

    MSG *msg = static_cast<MSG*>(message);
    if((msg->message == WM_HOTKEY) && (msg->wParam == 0))
    {
        qWarning() << "CTRL+ALT+1 pressed";
        if (this->isHidden())
        {
            setAttribute(Qt::WA_DontShowOnScreen, false);
            show();
        }
        else
        {
            setAttribute(Qt::WA_DontShowOnScreen, true);
            hide();
        }
        return true;
    }
    if((msg->message == WM_HOTKEY) && (msg->wParam == 1))
    {
        qWarning() << "CTRL+ALT+2 pressed";
        QCoreApplication::instance()->quit();
        return true;
    }
    if((msg->message == WM_HOTKEY) && (msg->wParam == 2))
    {
        qWarning() << "CTRL+ALT+3 pressed";
        dotaServerLogAreUpdate(DOTA_SERVER_LOG_FILE);
        return true;
    }
    return false;
}

void MainWindow::upplySettings()
{
    for(auto &i : objPlayers)
    {
        i->upplySettings();
        i->adjustSize();
    }
    adjustSize();
}
void MainWindow::setTrayIcon()
{

    connect(&actionSettingsOpen, &QAction::triggered, &settings, &SettingsWidget::show);
    connect(&actionQuitApplication, &QAction::triggered, QCoreApplication::instance(), &QCoreApplication::quit);
    connect(&actionAbout, &QAction::triggered, this, &MainWindow::aboutMessage);
    connect(&tray, &QSystemTrayIcon::messageClicked, &settings, &SettingsWidget::show);

    trayMenu.addAction(&actionSettingsOpen);
    trayMenu.addAction(&actionAbout);
    trayMenu.addSeparator();
    trayMenu.addAction(&actionQuitApplication);
    tray.setContextMenu(&trayMenu);
    tray.setToolTip(windowTitle());
    tray.setIcon(QIcon(":av/icon"));
    tray.show();
}

void MainWindow::aboutMessage()
{
    QString text = QString("<h4>%1</h4>Simple Dota2 helper<br>Author: %3<br>Version: %2<br>Site: <a style=\"color: grey;\" href='http://%4'>%4</a>")
            .arg("KnowYuorEnemy")
            .arg("0.1.2.2")
            .arg("saw_tooth")
            .arg("knowyourenemy.herokuapp.com");

    QMessageBox about("About", text,
                      QMessageBox::NoIcon,
                      QMessageBox::Ok,
                      QMessageBox::NoButton,
                      QMessageBox::NoButton,
                      nullptr,
                      Qt::FramelessWindowHint);

    QSize mSize = about.sizeHint();

    about.setTextFormat(Qt::RichText);
    about.setIconPixmap(QPixmap(":/av/icon").scaledToWidth(mSize.height() /2, Qt::SmoothTransformation));

    QRect screenRect = QDesktopWidget().screen()->rect();
    about.move(QPoint(screenRect.width()/2 - mSize.width()/2,
                        screenRect.height()/2 - mSize.height()/2));
    about.exec();
}
MainWindow::~MainWindow()
{

}
