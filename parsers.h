#pragma once

#define sample_length 3

#include "dom.h"
#include <array>
#include <map>
#include <string>


struct charcomp {

   bool operator( )(const char* str1, const char* str2) const
   {
      return std::strcmp(str1, str2) < 0;
   }
};

enum Critical
{
    UnkHero = -1
};

enum Match
{
    Unk = 0,
    Lose = 1,
    Win = 2,
    Leave = 3,

};

static const std::map <const char*, int, charcomp> heroes = {
    //#python>
    {
        "Anti-Mage", 1
    }, 
    {
        "Axe", 2
    }, 
    {
        "Bane", 3
    }, 
    {
        "Bloodseeker", 4
    }, 
    {
        "Crystal Maiden", 5
    }, 
    {
        "Drow Ranger", 6
    }, 
    {
        "Earthshaker", 7
    }, 
    {
        "Juggernaut", 8
    }, 
    {
        "Mirana", 9
    }, 
    {
        "Morphling", 10
    }, 
    {
        "Shadow Fiend", 11
    }, 
    {
        "Phantom Lancer", 12
    }, 
    {
        "Puck", 13
    }, 
    {
        "Pudge", 14
    }, 
    {
        "Razor", 15
    }, 
    {
        "Sand King", 16
    }, 
    {
        "Storm Spirit", 17
    }, 
    {
        "Sven", 18
    }, 
    {
        "Tiny", 19
    }, 
    {
        "Vengeful Spirit", 20
    }, 
    {
        "Windranger", 21
    }, 
    {
        "Zeus", 22
    }, 
    {
        "Kunkka", 23
    }, 
    {
        "Lina", 25
    }, 
    {
        "Lion", 26
    }, 
    {
        "Shadow Shaman", 27
    }, 
    {
        "Slardar", 28
    }, 
    {
        "Tidehunter", 29
    }, 
    {
        "Witch Doctor", 30
    }, 
    {
        "Lich", 31
    }, 
    {
        "Riki", 32
    }, 
    {
        "Enigma", 33
    }, 
    {
        "Tinker", 34
    }, 
    {
        "Sniper", 35
    }, 
    {
        "Necrophos", 36
    }, 
    {
        "Warlock", 37
    }, 
    {
        "Beastmaster", 38
    }, 
    {
        "Queen of Pain", 39
    }, 
    {
        "Venomancer", 40
    }, 
    {
        "Faceless Void", 41
    }, 
    {
        "Wraith King", 42
    }, 
    {
        "Death Prophet", 43
    }, 
    {
        "Phantom Assassin", 44
    }, 
    {
        "Pugna", 45
    }, 
    {
        "Templar Assassin", 46
    }, 
    {
        "Viper", 47
    }, 
    {
        "Luna", 48
    }, 
    {
        "Dragon Knight", 49
    }, 
    {
        "Dazzle", 50
    }, 
    {
        "Clockwerk", 51
    }, 
    {
        "Leshrac", 52
    }, 
    {
        "Nature's Prophet", 53
    }, 
    {
        "Lifestealer", 54
    }, 
    {
        "Dark Seer", 55
    }, 
    {
        "Clinkz", 56
    }, 
    {
        "Omniknight", 57
    }, 
    {
        "Enchantress", 58
    }, 
    {
        "Huskar", 59
    }, 
    {
        "Night Stalker", 60
    }, 
    {
        "Broodmother", 61
    }, 
    {
        "Bounty Hunter", 62
    }, 
    {
        "Weaver", 63
    }, 
    {
        "Jakiro", 64
    }, 
    {
        "Batrider", 65
    }, 
    {
        "Chen", 66
    }, 
    {
        "Spectre", 67
    }, 
    {
        "Ancient Apparition", 68
    }, 
    {
        "Doom", 69
    }, 
    {
        "Ursa", 70
    }, 
    {
        "Spirit Breaker", 71
    }, 
    {
        "Gyrocopter", 72
    }, 
    {
        "Alchemist", 73
    }, 
    {
        "Invoker", 74
    }, 
    {
        "Silencer", 75
    }, 
    {
        "Outworld Devourer", 76
    }, 
    {
        "Lycan", 77
    }, 
    {
        "Brewmaster", 78
    }, 
    {
        "Shadow Demon", 79
    }, 
    {
        "Lone Druid", 80
    }, 
    {
        "Chaos Knight", 81
    }, 
    {
        "Meepo", 82
    }, 
    {
        "Treant Protector", 83
    }, 
    {
        "Ogre Magi", 84
    }, 
    {
        "Undying", 85
    }, 
    {
        "Rubick", 86
    }, 
    {
        "Disruptor", 87
    }, 
    {
        "Nyx Assassin", 88
    }, 
    {
        "Naga Siren", 89
    }, 
    {
        "Keeper of the Light", 90
    }, 
    {
        "Io", 91
    }, 
    {
        "Visage", 92
    }, 
    {
        "Slark", 93
    }, 
    {
        "Medusa", 94
    }, 
    {
        "Troll Warlord", 95
    }, 
    {
        "Centaur Warrunner", 96
    }, 
    {
        "Magnus", 97
    }, 
    {
        "Timbersaw", 98
    }, 
    {
        "Bristleback", 99
    }, 
    {
        "Tusk", 100
    }, 
    {
        "Skywrath Mage", 101
    }, 
    {
        "Abaddon", 102
    }, 
    {
        "Elder Titan", 103
    }, 
    {
        "Legion Commander", 104
    }, 
    {
        "Techies", 105
    }, 
    {
        "Ember Spirit", 106
    }, 
    {
        "Earth Spirit", 107
    }, 
    {
        "Underlord", 108
    }, 
    {
        "Terrorblade", 109
    }, 
    {
        "Phoenix", 110
    }, 
    {
        "Oracle", 111
    }, 
    {
        "Winter Wyvern", 112
    }, 
    {
        "Arc Warden", 113
    }, 
    {
        "Monkey King", 114
    }, 
    {
        "Dark Willow", 119
    }, 
    {
        "Pangolier", 120
    }, 
    {
        "Grimstroke", 121
    }
//#python>
};

static const std::map <const char*, int, charcomp> match_status = {
    {"lost", Match::Lose/*0xFF0000*/},
    {"won", Match::Win/*0x3CB371*/},
    {"abandoned", Match::Leave/*0xC0C0C0*/}
};

struct match
{
    int hero_id = 0;
    Match match_status = Match::Unk;
    int kill = 0;
    int death = 0;
    int assistance = 0;
};

struct hero
{
    int matches = 0;
    int hero_id = 0;
    float winrate = 0;
    float kda = 0;
};

struct m_stat
{
    int win = 0;
    int lose = 0;
    float wr = 0;
};

class PeriodGameStatParser: public GumboParser
{

public:
    PeriodGameStatParser(const char *htmlPage);
    const bool statIsPresent() const;
    void getPartStat(m_stat& stats) const;

private:
    enum Values
    {
        Matches = 0,
        WR = 2
    };
    const int getWins() const;
    const int getLoses() const;
    const float getWR() const;
    const GumboNode *getContainer(Values el) const;
};

class PlayerInfoParser: public GumboParser
{
public:
    PlayerInfoParser(const char *htmlPage);
    void getSumStat(m_stat &stats) const;
    const char* getName() const;
    const bool isPrivate() const;

private:
    const GumboNode* gamesInfoContainer;
    const int getSumWins() const;
    const int getSumLosses() const;
    const float getSumWR() const;
};

class MatchesInfoParser: public GumboParser
{

private:
    std::vector<const GumboNode*> rows;
    void getContainer();
    const int get_hero(const GumboNode* el) const;
    const Match get_status(const GumboNode* el) const;
    void get_kda(const GumboNode* el, int& k, int& d, int& a) const;

public:
    MatchesInfoParser(const char *htmlPage);
    ~MatchesInfoParser();
    void getMatches(std::vector<const match*>& matches) const;
    const bool matchesIsPresent() const;
};

class HeroesInfoParser: public GumboParser
{

private:
    std::vector<const GumboNode*> rows;
    void getTableContainer();
    const int get_hero(const GumboNode* el) const;
    const float get_kda(const GumboNode* el) const;
    const int get_mathes(const GumboNode* el) const;
    const float get_wr(const GumboNode* el) const;
public:
    HeroesInfoParser(const char *htmlPage);
    ~HeroesInfoParser();
    void getLastHeroes(std::vector<const hero *>& heroes) const;
    const bool heroesIsPresent() const;
};
