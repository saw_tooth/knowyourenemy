# KnowYourEnemy

Dota2 helper

## Requirement

* qt 5.6+
* python 2/3+
* openssl (see, support libraly for your current qt version)
* qdarkstyle (as submodule)
* gumbo (as submodule) 

### Build

cmd commands
```
git clone ...
cd knowyourenemy

init submodules
git submodule init
git submodule update
cd hero_icon_parser

load heroes pictures and generate H-file and resourse qrc
python parser -l
python parser -g
python parser -r
cd ../
qmake -<*.pro>
make
```

### Settings for developers
Use logfile in same parent folder for biuld and project
```
--parent_folder
    |--project
    |--build_folder
    |--logfile.log
```

### Usage

|  keys | commands   |
| ------------ | ------------ |
| CTRL+ALT+1(numpad)  |  hide/open |
| CTRL+ALT+2(numpad)  |  close |
| CTRL+ALT+1(numpad)  |  update |

### Planned features

- [x] load info from dotabuff
- [ ] load info from alternatives resourses
- [x] trayicon
- [x] settings
- [ ] group (party) colored selection
- [ ] blacklist
- [ ] private view
- [ ] selectable data sourses (dotabuff/opendota etc)
- [ ] selectable view (current view and a few alternatives) (dotabuff/opendota etc)
- [ ] global hot keys (now keys are hardcoded)
