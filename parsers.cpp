#include "parsers.h"

int strseprint(const char* str_1, char separate)
{
    std::string res;
    for(int i = 0; str_1[i] != '\0'; i++)
    {
        if (str_1[i] != separate)
        {
            res.append(&str_1[i], 1);
        }
    }
    return std::atoi(res.c_str());
}

PlayerInfoParser::PlayerInfoParser(const char* htmlPage):GumboParser(htmlPage)
{
    gamesInfoContainer = findByClassName("header-content-secondary");
}

void PlayerInfoParser::getSumStat(m_stat& stats) const
{
    if (!isPrivate())
    {
        stats.lose = getSumLosses();
        stats.win = getSumWins();
        stats.wr = getSumWR();
    }
}

const char* PlayerInfoParser::getName() const
{
    const GumboNode* playerNameContainer = findByClassName("header-content-title");
    const char* pRet = getText(findByTagName(playerNameContainer, GUMBO_TAG_H1));
    return pRet ? pRet : "";
}

const int PlayerInfoParser::getSumWins() const
{
    const GumboNode* winsContainer = findByClassName(gamesInfoContainer, "wins");
    const char* pRet = getText(winsContainer);
    if (!pRet)
    {
        return 0;
    }
    return strseprint(pRet, ',');
}

const int PlayerInfoParser::getSumLosses() const
{
    const GumboNode* losesContainer = findByClassName(gamesInfoContainer, "losses");
    const char* pRet = getText(losesContainer);
    if (!pRet)
    {
        return 0;
    }
    return strseprint(pRet, ',');
}

const float PlayerInfoParser::getSumWR() const
{
    std::vector<const GumboNode*> cont;
    findAllByTagName(gamesInfoContainer, GUMBO_TAG_DD, cont);
    if (cont.empty()){ return 0.00; }
    const char* pRet = getText(cont[cont.size()-1]);
    if (!pRet)
    {
        return 0.00;
    }
    return std::atof(pRet);
}

const bool PlayerInfoParser::isPrivate() const
{
    const GumboNode* playerNameContainer = findByClassName("content-inner");
    const GumboNode* page_container = findByClassName(playerNameContainer, "fa fa-lock");
    return page_container ? true : false;
}

PeriodGameStatParser::PeriodGameStatParser(const char *htmlPage):GumboParser(htmlPage){}

void PeriodGameStatParser::getPartStat(m_stat &stats) const
{
    if (statIsPresent())
    {
        stats.lose = getLoses();
        stats.win = getWins();
        stats.wr = getWR();
    }
}

const int PeriodGameStatParser::getWins() const
{
    const GumboNode* cont = getContainer(Values::Matches);
    if (!cont)
    {
        return 0;
    }
    const char* winrate = getText(cont);
    return strseprint(winrate, ',');
}

const int PeriodGameStatParser::getLoses() const
{
    return getWins() * getWR() / 100;
}

const GumboNode* PeriodGameStatParser::getContainer(Values el) const
{
    const GumboNode* table_root = findByClassName("r-stats-grid");
    std::vector<const GumboNode*> ch;
    if (!table_root)
    {
        return nullptr;
    }
    std::vector<const GumboNode*> child;
    std::vector<const GumboNode*> values;

    findAllByClassName(table_root, "group group-soft", child);
    findAllByClassName(child[1], "kv", values);
    return values[el];
}

const float PeriodGameStatParser::getWR() const
{
    const GumboNode* cont = getContainer(Values::WR);
    if (!cont)
    {
        return 0.00;
    }
    const char* winrate = getText(findByClassName(cont, "color-stat-win"));
    return std::atof(winrate);
}

const bool PeriodGameStatParser::statIsPresent() const
{
    if (findByClassName("r-stats-grid"))
    {
        return true;
    }
    return false;
}

MatchesInfoParser::MatchesInfoParser(const char *htmlPage):GumboParser(htmlPage)
{
    getContainer();
}

MatchesInfoParser::~MatchesInfoParser(){}

void MatchesInfoParser::getContainer()
{
    const GumboNode* container = findByClassName("content-inner");
    const GumboNode* tableContainer = findByTagNameBFS(container, GUMBO_TAG_SECTION);
    const GumboNode* tableBody = findByTagNameBFS(tableContainer, GUMBO_TAG_SECTION);
    const GumboNode* matchesTable = findByTagName(tableBody, GUMBO_TAG_TBODY);
    findAllByTagNameBFS(matchesTable, GUMBO_TAG_TR, rows);
}

const bool MatchesInfoParser::matchesIsPresent() const
{
    if (rows.size() == 1 && findByClassName(rows[0], "cell-centered cell-explanation") != nullptr)
    {
        return false;
    }
    return true;
}

void MatchesInfoParser::getMatches(std::vector<const match*>& matches) const
{
    if (!matchesIsPresent())
    {
        return;
    }
    for (size_t i=0; (rows.size() > i && i < sample_length); i++)
    {
        match* record = new match;
        record->hero_id = get_hero(rows[i]);
        get_kda(rows[i], record->kill, record->death, record->assistance);
        record->match_status = get_status(rows[i]);
        matches.push_back(record);
    }
}

const int MatchesInfoParser::get_hero(const GumboNode* el) const
{
    const GumboNode* cell = findByClassName(el, "cell-large");
    const char* hero = getText(findByTagName(cell, GUMBO_TAG_A));
    auto idx = heroes.find(hero);
    if (idx != heroes.end())
    {
        return idx->second;
    }
    return UnkHero;
}

const Match MatchesInfoParser::get_status(const GumboNode *el) const
{
    std::vector<const GumboNode*> cells;
    findAllByTagNameBFS(el, GUMBO_TAG_TD, cells);
    const GumboNode* status = findByTagName(cells[3], GUMBO_TAG_A);
    const char* stat_text = getAttribute(status, "class");

    auto stat = match_status.find(stat_text);
    if (stat != match_status.end())
    {
        return static_cast<Match>(stat->second);
    }
    return Unk;
}

void MatchesInfoParser::get_kda(const GumboNode* el, int& k, int& d, int& a) const
{
    std::vector<const GumboNode*> stats;
    const GumboNode* container = findByClassName(el, "kda-record");

    findAllByTagNameBFS(container, GUMBO_TAG_SPAN, stats);
    if (stats.size() == 3)
    {
        k = std::atoi(getText(stats[0]));
        d = std::atoi(getText(stats[1]));
        a = std::atoi(getText(stats[2]));
    }
}

HeroesInfoParser::HeroesInfoParser(const char *htmlPage):GumboParser(htmlPage)
{
    getTableContainer();
}

HeroesInfoParser::~HeroesInfoParser(){}

void HeroesInfoParser::getTableContainer()
{

    const GumboNode* container = findByClassName("content-inner");
    const GumboNode* tableContainer = findByClassName(container, "sortable");
    const GumboNode* heroesTable = findByTagName(tableContainer, GUMBO_TAG_TBODY);
    findAllByTagNameBFS(heroesTable, GUMBO_TAG_TR, rows);
}

void HeroesInfoParser::getLastHeroes(std::vector<const hero*>& heroes) const
{
    if (!heroesIsPresent())
    {
        return;
    }
    for (size_t i=0; (rows.size() > i && i < sample_length); i++)
    {
        hero* record = new hero;
        record->hero_id = get_hero(rows[i]);
        record->kda = get_kda(rows[i]);
        record->matches = get_mathes(rows[i]);
        record->winrate = get_wr(rows[i]);
        heroes.push_back(record);
    }

}

const bool HeroesInfoParser::heroesIsPresent() const
{
    if (rows.size() == 1 && findByClassName(rows[0], "cell-centered cell-explanation") != nullptr)
    {
        return false;
    }
    return true;
}

const int HeroesInfoParser::get_hero(const GumboNode* el) const
{
    const GumboNode* ch = findByClassName(el, "cell-icon");
    const char* hero_name = getAttribute(ch,"data-value");

    auto idx = heroes.find(hero_name);
    if (idx != heroes.end())
    {
        return idx->second;
    }
    return UnkHero;
}

const float HeroesInfoParser::get_kda(const GumboNode* el) const
{
    std::vector<const GumboNode*> cols;
    findAllByTagName(el, GUMBO_TAG_TD, cols);
    const char* kda_str = getAttribute(cols[4],"data-value");
    return std::atof(kda_str);
}

const int HeroesInfoParser::get_mathes(const GumboNode* el) const
{
    std::vector<const GumboNode*> cols;
    findAllByTagName(el, GUMBO_TAG_TD, cols);
    const char* mathes_str = getAttribute(cols[2],"data-value");
    return std::atoi(mathes_str);
}

const float HeroesInfoParser::get_wr(const GumboNode* el) const
{
    std::vector<const GumboNode*> cols;
    findAllByTagName(el, GUMBO_TAG_TD, cols);
    const char* kda_str = getAttribute(cols[3],"data-value");
    return std::atof(kda_str);
}
