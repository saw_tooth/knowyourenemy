#include "views.h"
#include "settings.h"

HeroIcon::HeroIcon(QWidget* parent):QLabel(parent)
{}

void HeroIcon::setIcon(int idx)
{
    h_idx = idx;
    resize();
}

void HeroIcon::resize()
{
    if (h_idx)
    {
        QPixmap original(QString(":/%1").arg(h_idx));
        QPixmap scaled = original.scaledToWidth(this->width(), Qt::SmoothTransformation);
        setPixmap(scaled);
     }
}

void HeroIcon::resizeEvent(QResizeEvent* event)
{
    Q_UNUSED(event);
    if (pixmap())
    {
         resize();
    }

}

GameIcon::GameIcon(QWidget* parent):HeroIcon(parent)
{

}

void GameIcon::setStatus(int value)
{
    status = value;
    resize();
}

void GameIcon::resize()
{
    if (h_idx)
    {
        QPixmap original(QString(":%1").arg(h_idx));
        QPixmap scaled = original.scaledToWidth(this->width(), Qt::SmoothTransformation);
        QPainter p(&scaled);

        int height = scaled.height() / 10;
        p.fillRect(0, scaled.height() - height, scaled.width(), height, QColor(color_pallete[status]));
        setPixmap(scaled);
    }
}


LastGameView::LastGameView()
{
    ui();
}

void LastGameView::ui()
{
    QVBoxLayout* lCont = new QVBoxLayout;

    empty = new QLabel("no stat");
    container = new QWidget();
    icon = new GameIcon(container);
    kda = new QLabel(container);

    empty->setAlignment(Qt::AlignCenter);
    empty->setObjectName("nostat");

    container->setLayout(lCont);
    icon->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Fixed);

    kda->setObjectName("gameKDA");

    lCont->setSpacing(2);
    lCont->setContentsMargins(0,0,0,0);

    lCont->addWidget(icon);
    lCont->addWidget(kda);

    addWidget(container);
    addWidget(empty);
    setCurrentIndex(0);
}

void LastGameView::set(const match& value)
{
    if (currentIndex() != 0)
    {
        setCurrentIndex(0);
    }
    this->icon->setStatus(value.match_status);
    this->icon->setIcon(value.hero_id);
    this->kda->setText(QString("%1-%2-%3").arg(value.kill).arg(value.death).arg(value.assistance));
}

void LastGameView::setUI(const bool& g_on, const bool& kda)
{
    this->setHidden(!g_on);
    this->kda->setHidden(!kda);
}

void LastGameView::setEmpty()
{
    empty->setMinimumSize(container->size());
    setCurrentIndex(1);
}


MostHeroView::MostHeroView()
{
    ui();
}

void MostHeroView::ui()
{
    QVBoxLayout* lCont = new QVBoxLayout;

    empty = new QLabel("no stat");

    container = new QWidget();
    icon = new HeroIcon(container);
    kda = new QLabel(container);
    winrate = new QLabel(container);
    match_played = new QLabel(container);

    empty->setAlignment(Qt::AlignCenter);
    empty->setObjectName("nostat");

    container->setLayout(lCont);
    icon->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Fixed);

    kda->setObjectName("heroKDA");
    winrate->setObjectName("heroWR");
    match_played->setObjectName("heroMatches");

    lCont->setSpacing(2);
    lCont->setContentsMargins(0,0,0,0);

    lCont->addWidget(icon);
    lCont->addWidget(match_played);
    lCont->addWidget(kda);
    lCont->addWidget(winrate);

    addWidget(container);
    addWidget(empty);
    setCurrentIndex(0);
}

void MostHeroView::setEmpty()
{
    empty->setMinimumSize(container->size());
    setCurrentIndex(1);
}

void MostHeroView::set(const hero& value)
{
    if (currentIndex() != 0)
    {
        setCurrentIndex(0);
    }
    icon->setIcon(value.hero_id);
    match_played->setText(QString::number(value.matches));
    kda->setText(QString::number(value.kda, 'G', 3));
    winrate->setText(QString::number(value.winrate, 'G', 3).append("%"));
}

void MostHeroView::setUI(const bool& h_on, const bool& match, const bool& kda, const bool& wr)
{
    this->setHidden(!h_on);
    this->match_played->setHidden(!match);
    this->winrate->setHidden(!wr);
    this->kda->setHidden(!kda);
}


void Player::setName(const char* value)
{

    qlName->setText(value);
}

void Player::setSumWR(float value)
{
    lSummaryInfo[2]->setText(QString::number(value).append("%"));
}

void Player::setSumWin(int value)
{
    lSummaryInfo[0]->setText(QString::number(value));
}

void Player::setSumLose(int value)
{
    lSummaryInfo[1]->setText(QString::number(value));
}

void Player::setPartWR(float value)
{

    lPartialInfo[2]->setText(QString::number(value).append("%"));
}

void Player::setPartWin(int value)
{
    lPartialInfo[0]->setText(QString::number(value));
}

void Player::setPartLose(int value)
{
    lPartialInfo[1]->setText(QString::number(value));
}

void Player::updatePlayerInfo(PlayerInfoParser& parser)
{
    setName(parser.getName());
    setIsPrivate(parser.isPrivate());
    m_stat v;
    parser.getSumStat(v);
    setSumLose(v.lose);
    setSumWin(v.win);
    setSumWR(v.wr);
}

void Player::updatePeriodMatchesInfo(PeriodGameStatParser& parser)
{
    m_stat v;
    parser.getPartStat(v);
    setPartWR(v.wr);
    setPartLose(v.lose);
    setPartWin(v.win);
}

void Player::updateMatchesInfo(MatchesInfoParser& parser)
{

    std::vector<const match*> ro;

    parser.getMatches(ro);
    for (size_t i = 0; i < 3; i++)
    {
        if (i < ro.size())
        {
            setLastGamesItem(*ro[i], i);
        }
        else
        {
            lastGames[i]->setEmpty();
        }
    }

}

void Player::updateHeroesInfo(HeroesInfoParser& parser)
{
    std::vector<const hero*> ro;

    parser.getLastHeroes(ro);
    for (size_t i = 0; i < 3; i++)
    {
        if (i < ro.size())
        {
            setMostHeroesItem(*ro[i], i);
        }
        else
        {
            mostHeroes[i]->setEmpty();
        }
    }
}

void Player::setMostHeroesItem(const hero& value, int view_idx)
{
    mostHeroes[view_idx]->set(value);
}

void Player::setLastGamesItem(const match& value, int view_idx)
{
    lastGames[view_idx]->set(value);
}

bool Player::getIsPrivate() const
{
    return isPrivate;
}

void Player::setIsPrivate(bool value)
{
    isPrivate = value;
}

Player::Player(QWidget* parent):QWidget(parent)
{
    qlName = new QLabel(this);
    qlName->setObjectName("playerName");
    qlName->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Fixed);
    vlMain.setMargin(1);
    vlMain.setSpacing(2);
    vlMain.addWidget(qlName);

    ui();
    upplySettings();

}

void Player::upplySettings()
{
    QSettings set(INI_FILE, QSettings::IniFormat);
    bool m_on = set.value("MainView/display_view").toBool();
    bool sum = set.value("MainView/summary_view").toBool();
    bool part = set.value("MainView/period_view").toBool();

    bool g_on = set.value("GamesView/display_view").toBool();
    bool kda = set.value("GamesView/kda_view").toBool();

    bool h_on = set.value("HeroesView/display_view").toBool();
    bool h_wr = set.value("HeroesView/winrate_view").toBool();
    bool h_match = set.value("HeroesView/matches_view").toBool();
    bool h_kda = set.value("HeroesView/kda_view").toBool();

    for (int i=0; i<3; i++)
    {

        qlSummIndex->setHidden(!(m_on && sum));
        lSummaryInfo[i]->setHidden(!(m_on && sum));

        qlPartIndex->setHidden(!(m_on && part));
        lPartialInfo[i]->setHidden(!(m_on && part));

        lastGames[i]->setUI(g_on, kda);
        mostHeroes[i]->setUI(h_on, h_match, h_kda, h_wr);
    }
}

void Player::ui()
{
    QGridLayout* glPlayerInformation = new QGridLayout;
    QHBoxLayout* gllastGames = new QHBoxLayout;
    QHBoxLayout* glMostHeroes = new QHBoxLayout;

    gllastGames->setSpacing(2);
    glMostHeroes->setSpacing(2);

    qlSummIndex = new QLabel();
    qlPartIndex = new QLabel();

    qlSummIndex->setObjectName("index");
    qlPartIndex->setObjectName("index");

    qlSummIndex->setText(QChar(0x11,0x22));
    qlPartIndex->setText(QString("%1<sub>P</sub>").arg(QChar(0x11,0x22)));

    glPlayerInformation->addWidget(qlSummIndex,0,0,1,1, Qt::AlignLeft);
    glPlayerInformation->addWidget(qlPartIndex,1,0,1,1, Qt::AlignLeft);

    const char* ids[] = {"wins","loses", "wr"};

    for(int i=0;i < 3; i++)
    {
        lSummaryInfo[i] = new QLabel();
        lPartialInfo[i] = new QLabel();

        lSummaryInfo[i]->setObjectName(ids[i]);
        lPartialInfo[i]->setObjectName(ids[i]);

        glPlayerInformation->addWidget(lSummaryInfo[i], 0,1+i*2,1,2, Qt::AlignLeft);
        glPlayerInformation->addWidget(lPartialInfo[i], 1,1+i*2,1,2, Qt::AlignLeft);

        lastGames[i] = new LastGameView();
        mostHeroes[i] = new MostHeroView();

        gllastGames->addWidget(lastGames[i]);
        glMostHeroes->addWidget(mostHeroes[i]);
    }

    vlMain.addLayout(glPlayerInformation);
    vlMain.addLayout(gllastGames);
    vlMain.addLayout(glMostHeroes);
    vlMain.addStretch();
    setLayout(&vlMain);
}

#ifdef QT_DEBUG
void Player::debugInfo()
{

    qDebug() << "-------------------------";
    qDebug() << "ID: " << 1123;
    qDebug() << "NAME: " << qlName->text();
    qDebug() << "WIN: " << lSummaryInfo[0]->text();
    qDebug() << "LOSE: " << lSummaryInfo[1]->text();
    qDebug() << "WR: " << lSummaryInfo[2]->text();
    qDebug() << "PRIVATE: " << isPrivate;
    qDebug() << "-------------------------";
}
void Player::debugMatches(){}
void Player::debugHeroes(){}
#endif
