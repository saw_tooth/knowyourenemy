#include "dom.h"

GumboParser::GumboParser(const char *html)
{
    obj = gumbo_parse(html);
    document = obj->root;
}

void GumboParser::findAllByTagName(GumboTag tag, std::vector<const GumboNode *> &vec) const
{
    return findAllByTagName(document, tag, vec);
}
const GumboNode* GumboParser::findByTagName(const GumboNode* r, GumboTag tag) const
{
    const GumboVector* children = &r->v.element.children;
    for (size_t i = 0; i < children->length; i++)
    {
        const GumboNode* child = (const GumboNode* )children->data[i];
        if (child->type != GUMBO_NODE_ELEMENT) { continue; }
        if (child->v.element.tag == tag) { return child; }
        const GumboNode* pRet = findByTagName(child, tag);
        if (pRet) { return pRet; }
    }
    return nullptr;
}

const GumboNode* GumboParser::findByTagName(GumboTag tag) const
{
    return findByTagName(document, tag);
}
const GumboNode* GumboParser::findByTagNameBFS(const GumboNode* r, GumboTag tag) const
{
    std::queue<const GumboNode*> node_queue;
    node_queue.push(r);
    while (!node_queue.empty())
    {
        const GumboNode* node = node_queue.front();
        node_queue.pop();
        const GumboVector* children = &node->v.element.children;
        for (size_t i = 0; i < children->length; i++)
        {
            const GumboNode* child = (const GumboNode* )children->data[i];
            if (child->type != GUMBO_NODE_ELEMENT) { continue; }
            if (child->v.element.tag == tag)
            {
                return child;
            }
            else
            {
                node_queue.push(child);
            }
        }
    }
    return nullptr;
}

void GumboParser::findAllByTagNameBFS(const GumboNode* r, GumboTag tag, std::vector<const GumboNode *> &vec) const
{
    std::queue<const GumboNode*> node_queue;
    node_queue.push(r);
    while (!node_queue.empty())
    {
        const GumboNode* node = node_queue.front();
        node_queue.pop();
        const GumboVector* children = &node->v.element.children;
        for (size_t i = 0; i < children->length; i++)
        {
            const GumboNode* child = (const GumboNode* )children->data[i];
            if (child->type != GUMBO_NODE_ELEMENT) { continue; }
            node_queue.push(child);
            if (child->v.element.tag == tag)
            {
                vec.push_back(child);
            }
        }
    }
}

const GumboNode* GumboParser::findByTagNameBFS(GumboTag tag) const
{
    return findByTagNameBFS(document, tag);
}

void GumboParser::findAllByTagName(const GumboNode* r, GumboTag tag, std::vector<const GumboNode*> &vec) const
{
    const GumboVector* children = &r->v.element.children;
    for (size_t i = 0; i < children->length; i++)
    {
        const GumboNode* child = (const GumboNode* )children->data[i];
        if (child->type != GUMBO_NODE_ELEMENT) { continue; }
        if (child->v.element.tag == tag)
        {
            vec.push_back(child);
        }
        findAllByTagName(child, tag, vec);
    }
}

const GumboNode* GumboParser::findByClassName(const GumboNode* r, const char* cls_name) const
{
    const GumboVector* children = &r->v.element.children;
    const char* cl;
    for (size_t i = 0; i < children->length; i++)
    {

        const GumboNode* child = (const GumboNode* )children->data[i];
        if (child->type != GUMBO_NODE_ELEMENT) { continue;}
        if ((cl=getClass(child)) && !stricmp(cl,cls_name))
        {
            return child;
        }
        const GumboNode* pRet = findByClassName(child, cls_name);
        if (pRet) { return pRet; }
    }
    return nullptr;
}

const GumboNode* GumboParser::findByClassName(const char* cls_name) const
{
    return findByClassName(document, cls_name);
}

void GumboParser::findAllByClassName(const GumboNode* r, const char* cls_name, std::vector<const GumboNode*> &vec) const
{

    const GumboVector* children = &r->v.element.children;
    const char* cl;
    for (size_t i = 0; i < children->length; i++)
    {
        const GumboNode* child = (const GumboNode* )children->data[i];
        if (child->type != GUMBO_NODE_ELEMENT) { continue; }
        if ((cl = getClass(child)) && !stricmp(cl, cls_name))
        {
            vec.push_back(child);
        }
        findAllByClassName(child, cls_name, vec);
    }
}

void GumboParser::findAllByClassName(const char* cls_name, std::vector<const GumboNode*> &vec) const
{
    findAllByClassName(document, cls_name, vec);
}

const GumboNode* GumboParser::findById(const GumboNode* r, const char* id_name) const
{
    const GumboVector* children = &r->v.element.children;
    const char* id;
    for (size_t i = 0; i < children->length; i++)
    {

        const GumboNode* child = (const GumboNode* )children->data[i];
        if (child->type != GUMBO_NODE_ELEMENT) { continue;}
        if ((id=getId(child)) && !stricmp(id, id_name))
        {
            return child;
        }
        const GumboNode* pRet = findById(child, id_name);
        if (pRet) { return pRet; }
    }
    return nullptr;
}

const GumboNode* GumboParser::findById(const char* id_name) const
{
    return findById(document, id_name);
}

void GumboParser::findAllById(const GumboNode* r, const char* id_name, std::vector<const GumboNode*> &vec) const
{

    const GumboVector* children = &r->v.element.children;
    const char* id;
    for (size_t i = 0; i < children->length; i++)
    {
        const GumboNode* child = (const GumboNode* )children->data[i];
        if (child->type != GUMBO_NODE_ELEMENT) { continue; }
        if ((id = getId(child)) && !stricmp(id, id_name))
        {
            vec.push_back(child);
        }
        findAllById(child, id_name, vec);
    }
}

void GumboParser::findAllById(const char* id_name, std::vector<const GumboNode*> &vec) const
{
    findAllById(document, id_name, vec);
}

void GumboParser::findAllByTagNameBFS(GumboTag tag, std::vector<const GumboNode *> &vec) const
{
    findAllByTagNameBFS(document, tag, vec);
}

const char* GumboParser::getId(const GumboElement* el) const
{
    if (gumbo_get_attribute(&el->attributes, "id"))
    {
        return gumbo_get_attribute(&el->attributes, "id")->value;
    }
    return nullptr;
}
const char* GumboParser::getId(const GumboNode* node) const
{
    return getId(&node->v.element);
}

const char* GumboParser::getText(const GumboElement* el) const
{
    if (!el) { return nullptr; }
    const GumboNode* text_node = (const GumboNode* )el->children.data[0];
    return text_node->v.text.text;
}
const char* GumboParser::getText(const GumboNode* node) const
{
    if (!node) { return nullptr; }
    return getText(&node->v.element);
}

const char* GumboParser::getLink(const GumboElement* el) const
{
    if (!el) { return nullptr; }
    if (el->tag != GUMBO_TAG_A) { return nullptr; }
    return gumbo_get_attribute(&el->attributes, "href")->value;
}
const char* GumboParser::getLink(const GumboNode* node) const
{
    if (!node) { return nullptr; }
    return getLink(&node->v.element);
}

const char* GumboParser::getClass(const GumboElement* el) const
{
    if (gumbo_get_attribute(&el->attributes, "class"))
    {
        return gumbo_get_attribute(&el->attributes, "class")->value;
    }
    return nullptr;
}
const char* GumboParser::getClass(const GumboNode* node) const
{
    return getClass(&node->v.element);
}

const char* GumboParser::getAttribute(const GumboElement* el, const char* attr) const
{
    if (gumbo_get_attribute(&el->attributes, attr))
    {
        return gumbo_get_attribute(&el->attributes, attr)->value;
    }
    return nullptr;
}
const char* GumboParser::getAttribute(const GumboNode* node, const char* attr) const
{
    return getAttribute(&node->v.element, attr);
}

GumboParser::~GumboParser()
{
    gumbo_destroy_output(&kGumboDefaultOptions, obj);
    document = nullptr;
    obj = nullptr;
}

